const getcartItems = async () => {
  try {
    const response = await axios.get(
      "http://localhost:8080/api/user/getcartitems",
      {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      }
    );
    const items = response.data;
   // console.log(items) ;
    return items;
  } catch (error) {
    console.error(error);
  }
};

const CreateItemCarts =  (x) => {
  const row = document.createElement("TR");
  const product = document.createElement("TD");
  const ProductImage = document.createElement("img");
  ProductImage.src = `${x.img}`;
  ProductImage.className = "img-cart";
  product.appendChild(ProductImage);
  const description = document.createElement("TD");
  const descriptioncont = document.createElement("strong");
  const desc = document.createTextNode(`${x.name}`);
  descriptioncont.appendChild(desc);
  description.appendChild(descriptioncont);
  const quantity = document.createElement("TD");
  const formItem = document.createElement("form");
  formItem.className = "form-inline";
  const inputQty = document.createElement("input");
  inputQty.className = "form-control";
  inputQty.type = "text";
  inputQty.value = `${x.qty}`;
  const editButton = document.createElement("button");
  editButton.className = "btn btn-default";
  editButton.rel = "tooltip";
  const editIcon = document.createElement("i");
  editIcon.className = "fa fa-pencil";
  const removeButton = document.createElement("BUTTON");
  removeButton.className = "btn btn-primary";
  removeButton.addEventListener("click",  async function (e) {
    
      await removeItem(x._id);
    
    
  });

  const removeIcon = document.createElement("i");
  removeIcon.className = "fa fa-trash-o";
  removeButton.appendChild(removeIcon);
  editButton.appendChild(editIcon);
  formItem.appendChild(inputQty);
  formItem.appendChild(editButton);
  formItem.appendChild(removeButton);
  quantity.appendChild(formItem);
  const price = document.createElement("TD");
  const PriceText = document.createTextNode(`${x.price * x.qty}`);
  price.appendChild(PriceText);
  row.appendChild(product);
  row.appendChild(description);
  row.appendChild(quantity);
  row.appendChild(price);
  return row;
};
const DisplayItemCarts = async () => {
  const ArrayItems = await getcartItems();

  const container = document.getElementById("details");
  for (let i = 0; i < ArrayItems.cartItems.length; i++) {
    container.appendChild(CreateItemCarts(ArrayItems.cartItems[i]));
  }
};

const removeItem = async (id) => {
  const response = await axios({
    method: "post",
    url: "http://localhost:8080/api/user/cart/removeItem",
    data: {
      productId: id,
    },
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  });
   //console.log(response.data);
};
DisplayItemCarts();
const CalculAmountTotal = async () => {
  const ArrayItems = await getcartItems();
  let PriceTotal = 0;
  for (let i = 0; i < ArrayItems.cartItems.length; i++) {
    PriceTotal += ArrayItems.cartItems[i].qty * ArrayItems.cartItems[i].price;
  }
  //console.log(PriceTotal);
  return PriceTotal;
};
/*const addOrder = async () =>{
  const response = await axios({
      method: "post",
      url: "http://localhost:8080/api/addorder",
      data: {
        totalAmount:CalculAmountTotal(),
        items:[getCartItems().cartItems.prod]
      },
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    });
    console.log(response.data);
    return response.data;
   // 
}*/
const addOrder = async () => {
  if (localStorage.getItem("token") !== null) {
    const response = await axios({
      method: "post",
      url: "http://localhost:8080/api/addorder",
      data: {
        totalAmount: await CalculAmountTotal(),
        items: await GetTab(),
      },
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    });
    console.log(response.data);
    localStorage.setItem("orderId",response.data.order._id)
    location.href = "./order.html";
    return response.data;
  } else {
    location.href = "./signin.html";
  }
};

//

const GetTab = async () => {
  const ArrayItems = await getcartItems();
  const array = [];
  for (let i = 0; i < ArrayItems.cartItems.length; i++) {
    array.push({
      productId: ArrayItems.cartItems[i]._id,
      payablePrice: ArrayItems.cartItems[i].price,
      purchasedQty: ArrayItems.cartItems[i].qty,
    });
  }
  return array;
};
const tab = GetTab();
//console.log(tab);
const DisplayUserName=()=>{
  const userName=document.getElementById("username");
  const email=localStorage.getItem("email");
  const username   = email.substring(0, email.lastIndexOf("@"));
 userName.innerText=username;
}
const LogOut = async() =>{
  try {
   const response=await axios.post("http://localhost:8080/api/signout");
   console.log(response.data);
   localStorage.removeItem("token");
   localStorage.removeItem("email");
   location.replace('menu.html');
  } catch (error) {
    console.log(error);
  }
 
 
}
DisplayUserName();