
 const createItemOrder =(x,i)=>{
 const row=document.createElement('div');
 row.className="row mb-2 mb-sm-0 py-25";
 const c1=document.createElement('div');
 c1.className="d-none d-sm-block col-1";
 const c1text=document.createTextNode(i);
 c1.appendChild(c1text);
 const c2=document.createElement('div');
 c2.className="col-9 col-sm-5";
 const c2text=document.createTextNode(x.productId.name);
 c2.appendChild(c2text);
 const c3=document.createElement('div');
 c3.className="d-none d-sm-block col-2";
 const c3text=document.createTextNode(x.purchasedQty);
 c3.appendChild(c3text);
 const c4=document.createElement('div');
 c4.className="d-none d-sm-block col-2 text-95";
 const c4text=document.createTextNode(x.payablePrice);
 c4.appendChild(c4text);
 const c5=document.createElement('div');
 c5.className="col-2 text-secondary-d2";
 const c5text=document.createTextNode(x.purchasedQty*x.payablePrice);
 c5.appendChild(c5text);
 row.appendChild(c1);
 row.appendChild(c2);
 row.appendChild(c3);
 row.appendChild(c4);
 row.appendChild(c5);
 return row;
 }

const getOrder = async()=>{
  if(localStorage.getItem("token") !==null){
    try {
     const response =await axios.get(`http://localhost:8080/api/getorder/${localStorage.getItem("orderId")}`, {
       headers: {
         'Authorization': "Bearer " + localStorage.getItem("token"),
       }
     })
     const items = response.data;
   console.log(items) ;
   return items;
    } catch (error) {
     console.error(error);
    }
  }else{
    location.href = "./signin.html";
  }
   
   }
   
   const DisplayBill = async()=>{
   const order=await getOrder();
   let groupContainer=document.getElementById("OrdersContainer");
   for(let i=0;i<order.order.items.length;i++){
    groupContainer.appendChild(createItemOrder(order.order.items[i],i))
   }

   }
   const TotalAmount = async()=>{
    const order=await getOrder();
   // console.log(orders.orders[0]);
    let TotalPrice=0;
    for(let i=0;i<order.order.items.length;i++){
     TotalPrice += order.order.items[i].purchasedQty* order.order.items[i].payablePrice;
    }
    return TotalPrice;
 
    }
    const DisplaySubTotal = async()=>{
        const Price=document.getElementById("price");
        //const text=document.createTextNode(await TotalAmount())
        //Price.appendChild(text);
        Price.innerText="$"+await TotalAmount();
    }
    const DisplayTax = async()=>{
        const Tax=document.getElementById("tax");
        Tax.innerText="$"+(await TotalAmount()*0.1).toFixed(1);
    }
    const DisplayTotal= async()=>{
      const Total=document.getElementById("total");
      let total= (await TotalAmount()*0.1)+await TotalAmount();
        Total.innerText="$"+ total;
    } 
    let container=document.getElementById("information");
    
    const DisplayHeaderInformations = async ()=>{
      const order=await getOrder();
      let Information=document.getElementById("billiformation");
      Information.innerText=order.order._id;
    }
    const PrintToPdf =  ()=>{
      const element = document.getElementById("tobeprinted");
      const opt = {
        filename: 'myPage.pdf',
        margin: 2,
        jsPDF: {format: 'letter', orientation: 'portrait'}
      };
      // New Promise-based usage:
      html2pdf().set(opt).from(element).save();
      // Old monolithic-style usage:
      html2pdf(element, opt);
    }
    DisplayHeaderInformations();
    const DisplayInformations = async ()=>{
      const order=await getOrder();
      console.log(order.order.orderStatus[0].date);
      let content=`
    
      <div class="col-sm-6">

  <div class="text-95 col-md-6 align-self-start d-sm-flex justify-content-end">
      <hr class="d-sm-none" />
      <div class="text-grey-m2">
          <div class="mt-1 mb-2 text-secondary-m1 text-600 text-125">
              Invoice
          </div>

          <div class="my-2" ><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">ID:</span>${order.order._id} </div>

          <div class="my-2 "><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Issue Date:</span>${order.order.orderStatus[0].date} </div>

          <div class="my-2" ><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Status:</span> <span class="badge badge-warning badge-pill px-25">${order.order.orderStatus[0].type}</span></div>
      </div>
  </div>`
  container.innerHTML=content;
    }

    DisplayInformations();
   DisplayBill();
DisplaySubTotal();
DisplayTax();
DisplayTotal();