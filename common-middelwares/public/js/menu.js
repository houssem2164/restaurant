const getCategories = async () => {
  try {
    const response = await axios.get(
      "http://localhost:8080/api/category/getcategories"
    );

    const Items = response.data;

   // console.log(`GET: Here's the list of products`, Items);

    return Items;
  } catch (errors) {
    console.error(errors);
  }
};
const getProducts = async () => {
  try {
    const response = await axios.get(
      "http://localhost:8080/api/product/getall"
    );

    const Items = response.data;

  //  console.log(`GET: Here's the list of products`, Items);

    return Items;
  } catch (errors) {
    console.error(errors);
  }
};
const products = getProducts();

const addItemTocart = async (product) => {
  //event.preventDefault();
if(localStorage.getItem("token") !== null){
 
  const response = await axios({
    method: "post",
    url: "http://localhost:8080/api/user/cart/addtocart",
    data: {
      cartItems: {
        product: product._id,
        quantity: 1,
        price: product.price,
      },
    },
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  });
  console.log(response.data);
  location.reload()
}
else{
  location.href ="./signin.html";  
}
  //return response;
};
let listGroupContainer = document.getElementById("categoriesMenu");
const DisplayMenu =  async () => {
  const categories = await getCategories();
  const products = await getProducts();
  console.log(products[0].productImage);
  for (let i = 0; i < categories.length; i++) {
    const Category = CreateCategories(categories[i].name);  
    for (let j = 0; j < products.length; j++) {
      if (categories[i].name == products[j].category.name) {
        Category.appendChild(await CreateProducts(products[j]));
      }
    }
  }
};
const CreateCategories = (x) => {
  const ItemCategory = document.createElement("ul");
  ItemCategory.className="categoriesMenu";
  const NodeC = document.createTextNode(x);
  ItemCategory.appendChild(NodeC);
  listGroupContainer.appendChild(ItemCategory);
  return ItemCategory;
};
const CreateProducts = async (x) => {
  const ItemProduct = document.createElement("li");
  ItemProduct.className = "col-lg-6 menu-item filter-starters";
  const productImage = document.createElement("img");
  const Cart=document.createElement("button");
  Cart.className="cart-btn";
  Cart.type="submit";
  Cart.name="cart-btn";
  const NodeCart=document.createTextNode("Add-Item");
  Cart.appendChild(NodeCart);
  
  Cart.addEventListener('click',function(e) {
    if (e.target.tagName === "BUTTON") {
      
      addItemTocart(x);
     //location.reload();
  }});
  productImage.src = x.productImage;
  productImage.className = "menu-img";
  ItemProduct.appendChild(productImage);
  const ProductName = document.createElement("div");
  ProductName.className = "menu-content";
  const a = document.createElement("a");
  a.href = "#";
  //a.onclick=`return ${addItemTocart(x)};return true;`;
  const NodeA = document.createTextNode(x.name);
  const PriceItem = document.createElement("span");
  const NodeP = document.createTextNode("$" + x.price);
  PriceItem.appendChild(NodeP);
  PriceItem.appendChild(Cart);
  a.appendChild(NodeA);
  ProductName.appendChild(a);
  ProductName.appendChild(PriceItem);
  ItemProduct.appendChild(ProductName);
  const Description = document.createElement("div");
  Description.className = "menu-ingredients";
  const NodeD = document.createTextNode(x.description);
  Description.appendChild(NodeD);
  ItemProduct.appendChild(Description);
  return ItemProduct;
};

DisplayMenu();
const getCartItems = async()=>{
  if(localStorage.getItem("token") !==null){
 try {
  const response =await axios.get('http://localhost:8080/api/user/getcartitems', {
    headers: {
      'Authorization': "Bearer " + localStorage.getItem("token"),
    }
  })
  const items = response.data;
//console.log(items) ;
return items;
 } catch (error) {
  console.error(error);
 }
}
}


const  CountItemsCart = async() =>{
  let count=0;
const ArrayItems= await getCartItems();


for(let i=0;i<ArrayItems.cartItems.length;i++){
  count+=ArrayItems.cartItems[i].qty;
  console.log(count);
}


return count;
}
const DisplayCartQuantity =async()=>{
  if(localStorage.getItem("token") !==null){
 const aff= document.getElementById("lblCartCount");
 const NodeCount=document.createTextNode(await CountItemsCart());
 aff.appendChild(NodeCount);
 //location.reload();
  }
}

DisplayCartQuantity();

const login = async ()=>{
    event.preventDefault();
     
 const response=await axios.post("http://localhost:8080/api/signin",{
   email:email.value,
   password:password.value
 })

 localStorage.setItem("token",response.data.token);
 console.log("localStorage : " , localStorage.getItem("token"));
 localStorage.setItem("email",response.data.user.email);
 if(response.data.user.role=="user"){
   location.replace('menu.html');
   

   
 }
if(response.data.user.role=="admin"){
  location.replace('admin.html');
}
 };
 const navbar=document.getElementById("login-container");
const form = document.getElementById("login-form");
const redirecttostore= document.getElementById("tostore");
const DisplayNavbar = () =>{
   if(localStorage.getItem("token") !== null){
    navbar.style.visibility="hidden";
    
   }
   else{
    redirecttostore.onclick="return false;";
   }
 }
 const DisplayUserName=()=>{
   if(localStorage.getItem("email") !==null){
   const userName=document.getElementById("username");
   const email=localStorage.getItem("email");
   const username   = email.substring(0, email.lastIndexOf("@"));
  userName.innerText=username;
   }
 }
 const LogOut = async() =>{
   try {
    const response=await axios.post("http://localhost:8080/api/signout");
    console.log(response.data);
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    location.reload();
   } catch (error) {
     console.log(error);
   }
  
  
 }
 DisplayUserName();
 DisplayNavbar();