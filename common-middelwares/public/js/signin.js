


const email = document.getElementById("email");
const password = document.getElementById("password");
const passwordErrorField = document.getElementById('password-error-field');
const validatePassword = () => {
  if(password.validity.valid){
      // S'il n'y a pas d'erreur, on affiche
      // aucun message
      passwordErrorField.innerText = '';
      passwordErrorField.classList.remove('active');
      password.classList.remove('active');
  } else{
    // S'il y a une erreur, on affiche un
    //  message dépendant de l'erreur
    if(password.validity.valueMissing){
        passwordErrorField.innerText = 'Veuillez entrer votre mot de passe.';
    }
    else if(password.validity.tooShort){
        passwordErrorField.innerText = 'Votre mot de passe est trop court.';
    }
    passwordErrorField.classList.add('active');
    password.classList.add('active');
  }
}

const form = document.getElementById("login-form");
const checkConnected = () =>{
  if(localStorage.getItem("token") !=null){
     location.replace('menu.html');
  }
}
 checkConnected();
const login = async ()=>{
    event.preventDefault();
     validatePassword();
 const response=await axios.post("http://localhost:8080/api/signin",{
   email:email.value,
   password:password.value
 })
 console.log(response.data);
 localStorage.setItem("token",response.data.token);
 localStorage.setItem("email",response.data.user.email);
 console.log("localStorage : " , localStorage.getItem("token"));
 if(response.data.user.role=="user"){
   location.replace('menu.html');
 }
if(response.data.user.role=="admin"){
  location.replace('admin.html');
}
 };
   
