import mysql from 'promise-mysql'


export const connectionPromise = mysql.createPool({
    connectionLimit: process.env.BD_CONNECTION_LIMIT,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})
export default connectionPromise;