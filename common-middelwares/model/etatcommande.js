import { connectionPromise } from '../connectionsql.js'

export const getEtatCommande = async() => {

    let connection = await connectionPromise;
    let result = await connection.query(
        'SELECT*FROM etat_commande'

    );
    return result;


}