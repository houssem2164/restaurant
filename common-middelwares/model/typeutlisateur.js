import { connectionPromise } from '../connectionsql.js'

export const getTypeUitilisateur = async() => {

    let connection = await connectionPromise;
    let result = await connection.query(
        'SELECT*FROM type_utilisateur'

    );
    return result;
}