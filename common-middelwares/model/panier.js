import { connectionPromise } from '../connectionsql.js'

export const getPanier = async() => {

    let connection = await connectionPromise;
    let result = await connection.query(
        'SELECT*FROM panier'

    );
    return result;
}