import { connectionPromise } from '../connectionsql.js'

export const getProducts = async() => {

    let connection = await connectionPromise;
    let result = await connection.query(
        'SELECT*FROM produit'

    );
    return result;
}
export const addProducts = async(id_produit, nom, chemin_image, prix) => {
    let connection = await connectionPromise;
    await connection.query(

        `INSERT INTO produit (id_produit,nom,chemin_image,prix) 
        VALUES(?,?,?,?)`, [id_produit, nom, chemin_image, prix]


    );

}